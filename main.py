import time

from Population import Population

if __name__ == "__main__":
    target = 15
    rate_of_mutation = 0.06
    max_population = 1024
    current_iteration = 0

    population = Population(target, rate_of_mutation, max_population)

    population.display_intro()

    start_time = time.process_time()

    while not population.is_finished():
        if population.generations > len(population.population) * 100:
            population = Population(target, rate_of_mutation, max_population)
        # population.natural_selection()
        population.tournament_selection_probability()
        # population.tournament_selection()
        population.generate()
        population.calc_fitness()
        population.evaluate()
        current_iteration = current_iteration + 1
        population.display_realtime_info(current_iteration, time, start_time)

    population.display_info(current_iteration, time, start_time)
