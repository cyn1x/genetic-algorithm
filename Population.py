#
import os
import math
import random as random

from DNA import DNA


class Population:

    def __init__(self, population_target, mutation_rate, population_max):
        self.population = []
        self.matingPool = []
        self.generations = 0
        self.finished = False
        self.target = population_target
        self.mutationRate = mutation_rate
        self.perfectScore = 1
        self.best = []
        self.tournament_size = 32

        for i in range(population_max):
            dna = DNA()
            self.population.append(dna)

        self.calc_fitness()

    def calc_fitness(self):
        for i in range(len(self.population)):
            self.population[i].calc_fitness(self.target)

    def natural_selection(self):
        self.matingPool = []
        max_fitness = 0
        for i in range(len(self.population)):
            if self.population[i].fitness > max_fitness:
                max_fitness = self.population[i].fitness

        for i in range(len(self.population)):
            fitness = math.floor(self.population[i].fitness * 100)
            for j in range(fitness):
                self.matingPool.append(self.population[i])

    def tournament_selection(self):
        self.matingPool = []
        tournament_size = self.tournament_size
        dna = DNA()

        for i in range(len(self.population)):
            tournament_round = []
            for j in range(tournament_size):
                random_index = random.randint(0, len(self.population) - 1)
                while self.population[random_index] in tournament_round:
                    random_index = random.randint(0, len(self.population) - 1)
                tournament_round.append(self.population[random_index])
            best_fitness = 0
            fittest = dna
            for k in range(len(tournament_round)):
                if tournament_round[k].fitness > best_fitness:
                    best_fitness = tournament_round[k].fitness
                    fittest = tournament_round[k]

            self.matingPool.append(fittest)

    def tournament_selection_probability(self):
        self.matingPool = []
        tournament_size = self.tournament_size
        dna = DNA()
        first_score, first_place = 0, dna
        second_score, second_place = 0, dna
        third_score, third_place = 0, dna

        for i in range(len(self.population)):
            tournament_round = []
            p = 0.75
            probability = [p, p * (1 - p), p * (1 - p) ** 2]

            for j in range(tournament_size):
                random_index = random.randint(0, len(self.population) - 1)
                while self.population[random_index] in tournament_round:
                    random_index = random.randint(0, len(self.population) - 1)
                tournament_round.append(self.population[random_index])

                if tournament_round[j].fitness > first_score:
                    first_score = tournament_round[j].fitness
                    first_place = tournament_round[j]
                    self.matingPool.append(first_place)
                elif tournament_round[j].fitness > second_score:
                    second_score = tournament_round[j].fitness
                    first_place = tournament_round[j]
                elif tournament_round[j].fitness > third_score:
                    third_score = tournament_round[j].fitness
                    third_place = tournament_round[j]

            chance = random.uniform(0, 1)

            if chance <= probability[2]:
                self.matingPool.append(third_place)
            elif probability[2] < chance <= probability[1]:
                self.matingPool.append(second_place)
            elif probability[2] < probability[1] < chance:
                self.matingPool.append(first_place)

    def generate(self):
        for i in range(len(self.population)):
            a = math.floor(random.randint(0, len(self.matingPool) - 1))
            b = math.floor(random.randint(0, len(self.matingPool) - 1))
            partner_a = self.matingPool[a]
            partner_b = self.matingPool[b]
            while b == a:
                b = math.floor(random.randint(0, len(self.matingPool) - 1))
            child = partner_a.crossover(partner_b)
            child.mutate(self.mutationRate)
            self.population[i] = child
            self.generations = self.generations + 1

    def evaluate(self):
        record = 0.0
        index = 0
        for i in range(len(self.population)):
            if self.population[i].fitness > record:
                index = i
                record = self.population[i].last_score
        os.system('cls')

        self.best = self.population[index].genes
        if record == self.target:
            self.finished = True

    def display_realtime_info(self, current_iter, time, start_time):
        print("Tournament size: ", self.tournament_size)
        print("Current iteration: ", current_iter)
        print("Total generations: ", self.generations)
        print("Average fitness: ", round(self.get_average_fitness(), 2))
        print("Mutation rate: ", self.mutationRate)
        print("Current guess: ")
        print("Parking spot 1: ", self.best[0])
        print("Parking spot 2: ", self.best[1])
        print("Parking spot 3: ", self.best[2])
        print("Parking spot 4: ", self.best[3])
        print("Parking spot 5: ", self.best[4])
        print("Program execution time: %s seconds" % round((time.process_time() - start_time), 2))

    def display_info(self, iterations, time, start_time):
        port_macquarie = ""
        canadian_couple = ""
        for i in range(len(self.best)):
            for j in range(len(self.best)):
                if self.best[i][j] == "Port Macquarie":
                    port_macquarie = self.best[i][j - 2]
                if self.best[i][j] == "Canadian":
                    canadian_couple = self.best[i][j + 2]

        print("\nThe {0} was going to Port Macquarie.".format(port_macquarie))
        print("The {0} was hired by the individuals of Canadian descent.".format(canadian_couple))

        if (round(time.process_time() - start_time)) < 60:
            print("\nThe total execution time of the program was approximately {0} seconds"
                  .format(round((time.process_time() - start_time), 2)))
        else:
            run_time_minutes = round((time.process_time() - start_time) / 60)
            run_time_seconds = round((time.process_time() - start_time) % 60)
            print("\nThe total execution time of the program was approximately {0} minutes and {1} seconds"
                  .format(run_time_minutes, run_time_seconds))

        print("\nIn detail: \n"
              "\nThe {0} hired the {1} {2} and departed at {3} towards {4} from the first spot.".format
              (self.best[0][0], self.best[0][1], self.best[0][2], self.best[0][3], self.best[0][4]))
        print("The {0} hired the {1} {2} and departed at {3} towards {4} from the second spot.".format
              (self.best[1][0], self.best[1][1], self.best[1][2], self.best[1][3], self.best[1][4]))
        print("The {0} hired the {1} {2} and departed at {3} towards {4} from the third spot.".format
              (self.best[2][0], self.best[2][1], self.best[2][2], self.best[2][3], self.best[2][4]))
        print("The {0} hired the {1} {2} and departed at {3} towards {4} from the fourth spot.".format
              (self.best[3][0], self.best[3][1], self.best[3][2], self.best[3][3], self.best[3][4]))
        print("The {0} hired the {1} {2} and departed at {3} towards {4} from the fifth spot.".format
              (self.best[4][0], self.best[4][1], self.best[4][2], self.best[4][3], self.best[4][4]))

        input("\nPlease press <Enter> key to exit the program.")

    def display_intro(self):
        print("AI Strategy: Genetic Algorithm \n")
        print("The type of AI search strategy implementation is the genetic algorithm (GA). "
              "The GA belongs to the class of evolutionary algorithms (EA), and is a metaheuristic "
              "inspired by the process of natural selection. \n")
        print("For this particular example, natural selection will be replaced with tournament selection "
              "to effectively select the fittest member of each tournament round. If the current population "
              "is deemed to be unfit for the target, the population is reset for a fitter population to take "
              "its place. \n")
        print("In addition, the selected population will be 1024, with 1024 tournaments of 32 individuals. "
              "to accompany this, the set mutation rate has been set to 0.06 due to having a low population. \n")
        print("Finally, please note that the solution can be hard to predict due to the probability being used, "
              "and can sometimes longer than the previous run, or even shorter than the previous run. \n")
        input("Please press <Enter> key to execute the program.")

    def is_finished(self):
        return self.finished

    def get_average_fitness(self):
        total = 0
        for i in range(len(self.population)):
            total = total + self.population[i].fitness
        average_fitness = total / len(self.population) * 100
        return average_fitness
