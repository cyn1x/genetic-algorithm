# GeneticAlgorithm
Genetic algorithm written in Python

## What is it?
The GA belongs to the class of evolutionary algorithms (EA), and is a metaheuristic inspired by the process of
natural selection.

## What is it for?
I created this to learn more about AI, as I would like to implement an AI strategy in my chess engine.
