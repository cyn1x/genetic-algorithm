#
import math
import random as random
from itertools import chain


class DNA:
    def __init__(self):
        self.genes = self.generate_genome()
        self.last_score = 0
        self.fitness = 0

    def test(self):
        first = ["Canadian", "Blue", "Holden Barina", "5am", "Newcastle"]
        second = ["British", "Red", "Toyota Camry", "6am", "Tamworth"]
        third = ["French", "Black", "Nissan X-Trail", "8am", "Sydney"]
        fourth = ["Chinese", "White", "Hyundai Accent", "9am", "Gold Coast"]
        fifth = ["Indian", "Green", "Honda Civic", "7am", "Port Macquarie"]
        correct = [first, second, third, fourth, fifth]
        return correct

    def get_dna(self):
        car_models = ["Toyota Camry", "Hyundai Accent", "Holden Barina", "Nissan X-Trail", "Honda Civic"]
        car_colours = ["Black", "Blue", "Green", "Red", "White"]
        nationalities = ["British", "French", "Chinese", "Indian", "Canadian"]
        locations = ["Gold Coast", "Sydney", "Newcastle", "Tamworth", "Port Macquarie"]
        times = ["6am", "9am", "5am", "7am", "8am"]
        positions = ["First", "Second", "Third", "Fourth", "Fifth"]
        random.shuffle(car_colours)
        random.shuffle(car_models)
        random.shuffle(nationalities)
        random.shuffle(locations)
        random.shuffle(times)
        random.shuffle(positions)
        attributes = [nationalities, car_colours, car_models, times, locations]
        return attributes

    def generate_genome(self):
        attributes = self.get_dna()
        random_assortment = []
        for i in range(len(attributes)):
            random_assortment.append([attr[i] for attr in attributes])
        return random_assortment

    def calc_fitness(self, target):
        pos = 0
        score = 0
        for i in self.genes:
            score += i[0] == 'British' and i[3] == '6am' and i[2] == 'Toyota Camry'
            score += pos == 2 and i[1] == 'Black'
            score += i[2] == 'Hyundai Accent' and i[3] == '9am'
            score += pos < 4 and i[2] == 'Holden Barina' and i[1] == 'Blue' and self.genes[pos + 1][0] == 'British'
            score += pos > 0 and i[4] == 'Gold Coast' and self.genes[pos - 1][0] == 'French'
            score += i[2] == 'Nissan X-Trail' and i[4] == 'Sydney'
            score += pos > 0 and i[1] == 'Green' and self.genes[pos - 1][0] == 'Chinese'
            score += i[4] == 'Newcastle' and i[3] == '5am'
            score += pos > 0 and i[2] == 'Honda Civic' and i[3] == '7am' and self.genes[pos - 1][4] == 'Gold Coast'
            score += i[1] == 'Red' and i[4] == 'Tamworth'
            score += pos < 4 and i[1] == 'White' and self.genes[pos + 1][3] == '7am'
            score += pos == 4 and i[0] == 'Indian'
            score += i[1] == 'Black' and i[3] == '8am'
            score += pos > 0 and i[0] == 'Indian' and self.genes[pos - 1][0] == 'Chinese'
            score += i[4] == 'Tamworth' and i[3] == '6am'
            pos += 1
        duplicates = list(chain.from_iterable(self.genes))
        num_duplicates = len(set(duplicates))
        difference = (25 - num_duplicates)
        if num_duplicates < 25:
            score = score - difference

        self.last_score = score
        self.fitness = (score / target)

    def crossover(self, partner):
        child = DNA()
        midpoint = math.floor(len(self.genes) - 1)

        for i in range(len(self.genes)):
            for j in range(len(self.genes)):
                if i > midpoint:
                    child.genes[j][i] = partner.genes[j][i]
                else:
                    child.genes[j][i] = self.genes[j][i]

        return child

    def mutate(self, mutation_rate):
        for i in range(len(self.genes)):
            for j in range(len(self.genes)):
                probability = random.uniform(0, 1)
                if probability < mutation_rate:
                    self.genes[j][i] = self.generate_genome()[j][i]
